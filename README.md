# cardif_datalab_torchsimple

Project build on top of pytorch, in order to simplify training, boost the performance and implement the best practices in deep learning

Public projects considered: 
1. fastai https://github.com/fastai/fastai
2. torchsample https://github.com/ncullen93/torchsample
3. kekas: https://github.com/belskikh/kekas


Interesting ideas should be taken into account: 
1. super-convergence ideas: learning rate finder, one cycle learning rate, freeze, unfreezz layers 
https://arxiv.org/pdf/1803.09820.pdf 

How to find a good learning rate: \
https://sgugger.github.io/how-do-you-find-a-good-learning-rate.html

OneCycle Learning rate: \
https://sgugger.github.io/the-1cycle-policy.html#the-1cycle-policy

2. Mixup Training: https://arxiv.org/pdf/1710.09412.pdf

3. Distlling the Knowledge: https://arxiv.org/abs/1503.02531



